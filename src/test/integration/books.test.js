const { expect } = require("chai");
const req = require("supertest");
const server = require("../../server");

describe("/api/v1/books", () => {

  describe("GET", () => {
    describe("200 - Success", () => {
      let res;

      before("set res", async () => {
        res = await req(server).get("/api/v1/books").set("x-source-id", "1");
      });

      it("should return 200 status code", () => {
        expect(res.status).to.equal(200);
      });
    });
    describe("400 - Invalid Request", () => {
      let res;

      before("set res", async () => {
        res = await req(server).get("/api/v1/books");
      });

      it("should return 400 status code", () => {
        expect(res.status).to.equal(400);
      });

      it("should return 400 response body", () => {
        const resBody = {
          message: "Invalid Request",
          parameter: "x-source-id",
          in: "headers",
          status: 400
        };
        expect(res.body).to.deep.equal(resBody);
      });
    });
  });

  describe("POST", () => {
    describe("200 - Success", () => {
      let res;

      before("set res", async () => {
        const reqBody = {
          title: "House Of Leaves",
          author: "Mark Z. Danielewski"
        };
        res = await req(server).post("/api/v1/books").set("x-source-id", "1").send(reqBody);
      });

      it("should return 200 status code", () => {
        expect(res.status).to.equal(200);
      });
    });
    describe("400 - Invalid Request Headers", () => {
      let res;

      before("set res", async () => {
        res = await req(server).post("/api/v1/books");
      });

      it("should return 400 status code", () => {
        expect(res.status).to.equal(400);
      });

      it("should return 400 response body", () => {
        const resBody = {
          message: "Invalid Request",
          parameter: "x-source-id",
          in: "headers",
          status: 400
        };
        expect(res.body).to.deep.equal(resBody);
      });
    });
    describe("400 - Invalid Request Body", () => {
      let res;

      before("set res", async () => {
        res = await req(server).post("/api/v1/books").set("x-source-id", "1").send({});
      });

      it("should return 400 on missing property: author", () => {
        const resBody = {
          message: "Invalid Request",
          parameter: "author",
          in: "body"
        };
        expect(res.status).to.equal(400);
        expect(res.body).to.deep.equal(resBody);
      });
    });    
  });
});