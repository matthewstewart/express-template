const { expect } = require("chai");
const req = require("supertest");
const server = require("../../server");

describe("/api/v1/", () => {
  describe("GET", () => {
    let res;

    before("set res", async () => {
      res = await req(server).get("/api/v1/").set("x-source-id", "1");
    });

    it("should return 200 status code", () => {
      expect(res.status).to.equal(200);
    });

  });
});  