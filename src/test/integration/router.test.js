const { expect } = require("chai");
const req = require("supertest");
const server = require("../../server");

describe("Router Middleware", () => {

  describe("404 Route Middleware", () => {
    let res;

    before("set res", async () => {
      res = await req(server).get("/invalid-route").set("x-source-id", "1");
    });

    it("should return 404 status code", () => {
      expect(res.status).to.equal(404);
    });

    it("should return 404 response body", () => {
      const resBody = {
        status: 404,
        message: "Resource not found",
        parameter: "GET /invalid-route",
        in: "router"
      };
      expect(res.body).to.deep.equal(resBody);
    });
  });
});