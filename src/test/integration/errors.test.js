const { expect } = require("chai");
const req = require("supertest");
const server = require("../../server");
const { InvalidRequestError, NotFoundError, InternalServerError } = require("../../utilities/errors");

describe("Errors", () => {

  describe("400 - InvalidRequestError", () => {
    it("should return response", () => {
      const error = new InvalidRequestError({
        parameter: "x-source-id",
        in: "header"
      });
      const errorRes = error.response();
      const res = {
        status: 400,
        message: "Invalid Request",
        parameter: "x-source-id",
        in: "header"
      };
      expect(errorRes).to.deep.equal(res);
    });
  });

  describe("404 - NotFoundError", () => {
    it("should return response", () => {
      const error = new NotFoundError({
        parameter: "GET /invalid",
        in: "router"
      });
      const errorRes = error.response();
      const res = {
        status: 404,
        message: "Resource not found",
        parameter: "GET /invalid",
        in: "router"
      };
      expect(errorRes).to.deep.equal(res);
    });  
  });

  describe("500 - InternalServerError", () => {
    it("should return response", () => {
      const error = new InternalServerError({
        parameter: "GET /invalid",
        in: "router"
      });
      const errorRes = error.response();
      expect(errorRes.status).to.equal(500);
      expect(errorRes.message).to.equal("Internal Server Error");
      expect(errorRes).to.haveOwnProperty("errorMessage");
      expect(errorRes).to.haveOwnProperty("errorStack");
    });    
  });
});