const { InvalidRequestError } = require("../utilities/errors");

module.exports.getBooks = (req, res) => {
  res.status(200).json({
    message: "OK"
  });
};

module.exports.postBooks = (req, res) => {
  try {
    const { author } = req.body;
    if (!author) {
      const error = new InvalidRequestError("author", "body");
      error.parameter = "author";
      error.in = "body";
      error.status = 400;
      throw error;
    }
    res.status(200).json({
      message: "OK"
    });
  } catch (error) {
    res.status(error.status).json({
      message: error.message,
      parameter: error.parameter,
      in: error.in
    });
  }
};