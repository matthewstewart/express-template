require("dotenv").config();
const { API_PORT } = process.env;
const express = require("express");
const app = express();
const router = express.Router();

require("./middleware/app")(app);
require("./routes")(router);
app.use("/api/v1", router);
require("./middleware/router")(app, router);

app.listen(API_PORT, () => {
  console.log(`server listening on localhost:${API_PORT}`);
});

module.exports = app;