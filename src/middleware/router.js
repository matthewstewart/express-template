const { InternalServerError, NotFoundError } = require("../utilities/errors");

module.exports = (app, router) => {

  // 404 - Route Not Found
  app.use((req, res, next) => {
    const error = new NotFoundError({
      parameter: `${req.method} ${req.url}`,
      in: "router"
    });
    return res.status(error.status).json(error.response());
  }); 

};