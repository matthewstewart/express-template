const express = require("express");
const { InvalidRequestError } = require("../utilities/errors");

module.exports = (app) => {

  // parse json in request body
  app.use(express.json());

  // app level header validation
  app.use((req, res, next) => {
    if (!req.headers["x-source-id"]) {
      const error = new InvalidRequestError({
        parameter: "x-source-id",
        in: "headers"
      });
      return res.status(error.status).json(error.response());
    } else {
      next();
    }
  });

  // timer logger
  app.use((req, res, next) => {
    const startTime = process.hrtime();
    res.on("finish", () => {
      const endTime = process.hrtime(startTime);
      const timer = `${endTime[0]}s ${endTime[1] / 1e6}ms`;
      const message = `${req.method} ${req.url} - ${timer}`;
      console.log(message);
    });
    next();
  });
 
};