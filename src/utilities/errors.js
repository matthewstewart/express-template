class InvalidRequestError extends Error {

  constructor(props) {
    super();
    this.message = "Invalid Request";
    this.status = 400;
    this.parameter = props.parameter;
    this.in = props.in;
  } 

  response() {
    return {
      message: this.message,
      status: this.status,
      parameter: this.parameter,
      in: this.in 
    };
  }
}

class NotFoundError extends Error {

  constructor(props) {
    super();
    this.message = "Resource not found";
    this.status = 404;
    this.parameter = props.parameter;
    this.in = props.in;
  } 

  response() {
    return {
      message: this.message,
      status: this.status,
      parameter: this.parameter,
      in: this.in 
    };
  }
}

class InternalServerError extends Error {

  constructor(err) {
    super();
    this.message = "Internal Server Error";
    this.status = 500;
    this.errorMessage = err.message;
    this.errorStack = err.stack;
  } 

  response() {
    return {
      errorMessage: this.errorMessage,
      errorStack: this.errorStack,
      status: this.status,
      message: this.message
    };
  }
}

module.exports = {
  InvalidRequestError,
  NotFoundError,
  InternalServerError
};