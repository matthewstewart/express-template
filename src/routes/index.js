module.exports = (router) => {
  require("./books")(router);
  require("./landing")(router);
};