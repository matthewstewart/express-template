module.exports = (router) => {
  
  const { getBooks, postBooks } = require("../controllers/books");
  router
  .get("/books", getBooks)
  .post("/books", postBooks);

};