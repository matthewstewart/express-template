module.exports = (router) => {
  
  const { getLanding } = require("../controllers/landing");
  router.get("/", getLanding);

};