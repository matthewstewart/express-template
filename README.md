# Express Template
[![pipeline status](https://gitlab.com/matthewstewart/express-template/badges/master/pipeline.svg)](https://gitlab.com/matthewstewart/express-template/commits/master)
[![coverage report](https://gitlab.com/matthewstewart/express-template/badges/master/coverage.svg)](https://gitlab.com/matthewstewart/express-template/commits/master)

An API written for test coverage.

## Install

### Clone Project
```
git clone https://gitlab.com/matthewstewart/express-template
```

### Install Node Package Dependencies
```
npm i 
```

## Test
To run the test suite:
```
npm test
```